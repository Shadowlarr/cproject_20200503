#include <iostream>

using namespace std;

class Animal
{
public:
	Animal() {};

	virtual void Voice()
	{
		cout << "Hello World!" << '\n';
	}
};

class Cat : public Animal
{
public:
	Cat() {};

	void Voice() override
	{
		cout << "Cat says - Nya!" << '\n';
	}
};

class Dog : public Animal
{
public:
	Dog() {};

	void Voice() override
	{
		cout << "Dog says - Woof!" << '\n';
	}
};

class Horse : public Animal
{
public:
	Horse() {};

	void Voice() override
	{
		cout << "Horse says - Neigh!" << '\n';
	}
};

int main()
{
	const int i = 4;
	Animal** Beast = new Animal*[i];

	Beast[0] = new Cat;
	Beast[1] = new Dog;
	Beast[2] = new Horse;
	Beast[3] = new Animal;

	for (int j = 0; j < i; ++j)
	{
		Beast[j]->Voice();
	}
	delete[] Beast;
}